package com.ictcampus.lab.base.service.model;

import lombok.Builder;
import lombok.Value;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Value
@Builder
public class World {
	Long id;
	String name;
	String system;
}
