package com.ictcampus.lab.base.service.mapper;

import com.ictcampus.lab.base.data.Entity.WorldEntity;
import com.ictcampus.lab.base.service.model.World;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Mapper
public interface WorldServiceMapStruct {
	World toWorld( WorldEntity worldEntity );

	List<World> toWorld( List<WorldEntity> worldEntity );

	WorldEntity toWorldEntity( World world );
}
