package com.ictcampus.lab.base.control.mapper;

import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.service.model.World;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Mapper
public interface WorldControllerMapStruct {
	WorldResponse toWorldResponse( World world );

	List<WorldResponse> toWorldResponse( List<World> worlds );

	@Mapping( target = "id", ignore = true )
	World toWorld( WorldRequest worldRequest );
}
