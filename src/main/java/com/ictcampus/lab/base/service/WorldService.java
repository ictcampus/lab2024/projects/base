package com.ictcampus.lab.base.service;

import com.ictcampus.lab.base.data.Entity.WorldEntity;
import com.ictcampus.lab.base.data.WorldRepository;
import com.ictcampus.lab.base.service.mapper.WorldServiceMapStruct;
import com.ictcampus.lab.base.service.model.World;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Service
@AllArgsConstructor
public class WorldService {
	private WorldRepository worldRepository;
	private WorldServiceMapStruct worldServiceMapStruct;

	public World getWorld(long id) {
		WorldEntity worldEntity = worldRepository.findById( id );

		return worldServiceMapStruct.toWorld(worldEntity);
	}

	public List<World> getWorlds() {
		List<WorldEntity> worldEntity = worldRepository.findAll( );

		return worldServiceMapStruct.toWorld(worldEntity);
	}

	public Long createWorld(World world) {
		return worldRepository.create( worldServiceMapStruct.toWorldEntity(world) );
	}

	public void updateWorld(long id, World world) {
		worldRepository.update( id, worldServiceMapStruct.toWorldEntity(world) );
	}

	public void deleteWorld(long id) {
		worldRepository.delete( id );
	}
}
