package com.ictcampus.lab.base.data;

import com.ictcampus.lab.base.data.Entity.WorldEntity;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Types;
import java.util.Arrays;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@AllArgsConstructor
@Repository
public class WorldRepository {
	private JdbcTemplate jdbcTemplate;

	public List<WorldEntity> findAll() {
		return jdbcTemplate.query( "SELECT * FROM world", new BeanPropertyRowMapper<>( WorldEntity.class ) );
	}

	public WorldEntity findById( long id ) {
		return jdbcTemplate.queryForObject( "SELECT * FROM world WHERE id = ?", new BeanPropertyRowMapper<>( WorldEntity.class ), id );
	}

	@Transactional
	public long create( WorldEntity world ) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		PreparedStatementCreatorFactory preparedStatementCreatorFactory = new PreparedStatementCreatorFactory(
				"INSERT INTO world (name, system) VALUES (?, ?)",
				Types.VARCHAR, Types.VARCHAR
		);
		preparedStatementCreatorFactory.setReturnGeneratedKeys( true );

		PreparedStatementCreator preparedStatementCreator = preparedStatementCreatorFactory.newPreparedStatementCreator(
				Arrays.asList( world.getName(), world.getSystem() ) );


		jdbcTemplate.update( preparedStatementCreator, generatedKeyHolder );

		return generatedKeyHolder.getKey().longValue();
	}

	public int update( long id, WorldEntity world ) {
		return jdbcTemplate.update( "UPDATE world SET name=?, system=? WHERE id=?", world.getName(), world.getSystem(), id );
	}

	public int delete( long id ) {
		return jdbcTemplate.update( "DELETE FROM world WHERE id=?", id );
	}
}
