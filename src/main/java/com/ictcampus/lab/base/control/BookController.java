package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.BookResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since X.X.X TODO Add Class Version
 */

@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
public class BookController {

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooks() {
		List<BookResponse> list = new ArrayList<>();

		BookResponse bookResponse = BookResponse.builder()
				.id(1L)
				.title("titolo")
				.build();

		list.add(bookResponse);
		return list;
	}
}
