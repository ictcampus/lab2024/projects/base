package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.mapper.WorldControllerMapStruct;
import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.service.WorldService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/worlds" )
@AllArgsConstructor
public class WorldController {
	private WorldControllerMapStruct worldControllerMapStruct;
	private WorldService worldService;

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<WorldResponse> getWorlds() {
		return worldControllerMapStruct.toWorldResponse( worldService.getWorlds() );
	}

	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public WorldResponse getWorld(
			@PathVariable(name = "id") Long id
	) {
		//return worldControllerMapStruct.toWorldResponse( worldService.getWorld(id) );
		return WorldResponse.builder().build();
	}

	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Long createWorld(
			@RequestBody WorldRequest worldRequest
	) {
		return worldService.createWorld( worldControllerMapStruct.toWorld(worldRequest) );
	}

	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void editWorld(
			@PathVariable(name = "id") Long id,
			@RequestBody WorldRequest worldRequest
	) {
		worldService.updateWorld( id, worldControllerMapStruct.toWorld(worldRequest) );
	}

	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void deleteWorld(
			@PathVariable(name = "id") Long id
	) {
		worldService.deleteWorld( id );
	}
}
