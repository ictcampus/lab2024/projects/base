package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since X.X.X TODO Add Class Version
 */

@Value
@Builder
@Jacksonized
public class BookResponse {
	Long id;
	String title;
	String author;
	String ISDB;
}
