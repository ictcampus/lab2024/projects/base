CREATE SCHEMA IF NOT EXISTS baseproject;
SET SCHEMA baseproject;

CREATE TABLE world (id bigint auto_increment, name VARCHAR(50) NOT NULL, system VARCHAR(255) NOT NULL);
